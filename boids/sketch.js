const boids = [];

function setup() {
	createCanvas(1000, 800);
	for (let i=0; i<64; i++) {
		boids.push(new Boid(i));
	}
}

function draw() {
	background(51);

	//draw triangle on each boid in direction is facing
	boids.forEach(boid => {
		boid.update(boids);
		boid.flock
		boid.draw();
	});
}
