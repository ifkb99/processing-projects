class Boid {
	constructor(idx) {
		this.pos = createVector(random(100, 900), random(100, 700));
		this.vel = p5.Vector.random2D();
		this.vel.setMag(random(2, 4));
		this.accel = createVector();
		this.idx = idx;
		this.view = 50;
		this.viewAngleRight = 3 * QUARTER_PI;
		this.viewAngleLeft = 5 * QUARTER_PI;
		this.maxSpeed = 5;
		this.maxAccel = 0.2;
	}

	getBoidsInRange(allBoids) {
		const boids = [];
		allBoids.forEach((boid, idx) => {
			const posDiff = p5.Vector.sub(this.pos, boid.pos);
			if (idx !== this.idx
				&& abs(posDiff.x) <= this.view
				&& abs(posDiff.y) <= this.view) {
					const angleDiff = abs(this.vel.heading() - posDiff.heading());
					if (angleDiff > this.viewAngleRight || angleDiff < this.viewAngleLeft) {
						boids.push(boid);
						//debug
						//line(this.pos.x, this.pos.y, boid.pos.x, boid.pos.y);
						//stroke(8 % this.idx);
					}
				}
		});
		return boids;
	}

	align(boids) {
		const avgAlignment = createVector();
		
		boids.forEach(boid => avgAlignment.add(boid.vel));

		if (boids.length > 0) {
			avgAlignment.div(boids.length);
			avgAlignment.setMag(this.maxSpeed);
			avgAlignment.sub(this.vel);
			avgAlignment.limit(this.maxAccel);
		}
		return avgAlignment;
	}

	flock(boids) {
		//keeps boids towards center of mass of flock
		const avgPos = createVector();

		boids.forEach(boid => avgPos.add(boid.pos));

		if (boids.length > 1) {
			avgPos.div(boids.length);
			avgPos.sub(this.pos);
			avgPos.setMag(this.maxSpeed);
			avgPos.sub(this.vel);
			avgPos.limit(this.maxAccel);
		}

		return avgPos;
	}

	space(boids) {
		const turn = createVector();

		let numBoids = 0;
		boids.forEach(boid => {
			const d = dist(this.pos.x, this.pos.y, boid.pos.x, boid.pos.y);
			if (d <= 50 && d > 0) {
				turn.add(
					p5.Vector.sub(this.pos, boid.pos)
						 	 .div(d * d)
				);
				numBoids++;
			}
		});

		if (numBoids > 1) {
      		turn.div(numBoids);
			turn.setMag(this.maxSpeed);
      		turn.sub(this.vel);
      		turn.limit(this.maxForce);
    	}

		
		return turn;
	}

	edge() {
		if (this.pos.x < 0) {
			this.pos.x = width;
		} else if (this.pos.x > width) {
			this.pos.x = 0;
		}
		if (this.pos.y < 0) {
			this.pos.y = height;
		} else if (this.pos.y > height) {
			this.pos.y = 0;
		}
	}

	update(allBoids) {
		const boids = this.getBoidsInRange(allBoids);
		this.accel.add(this.align(boids));
		this.accel.add(this.flock(boids));
		this.accel.add(this.space(boids));

		this.accel.limit(this.maxAccel);
		this.vel.limit(this.maxSpeed);

		//add acceleration
		this.pos.add(this.vel);
		this.vel.add(this.accel);
		this.accel.mult(0);

		this.edge();
	}

	drawBoid() {
		//draws triangle according to proper heading
		const head = this.vel.heading();
		const sinHead = 3 * sin(head);
		const cosHead = 5 * cos(head);

		triangle(
			//bottom left
			this.pos.x - sinHead, this.pos.y + cosHead,
			//top mid
			this.pos.x + sinHead, this.pos.y - cosHead,
			//bottom right
			this.pos.x + sinHead, this.pos.y + cosHead
		);
	}

	draw() {
		strokeWeight(4);
		stroke(255);
		this.drawBoid();
	}
}
