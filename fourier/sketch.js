let time = 0; //sampling rate
let wave = [];
let waveLen = 0;
const iters = 8;
let period;

function setup() {
	createCanvas(1200, 800);
	period = PI * 2;
}

function draw() {
	background(0);
	translate(400, 400);

	let x=0;
	let y=0;

	for (let n=1; n <= iters; n++) {
		const coeff = (n * PI) / (period / 2);
		const prevX = x;
		const prevY = y;
		const radius = 128 * (1/coeff);
		x += radius * cos(coeff * time);
		y += radius * sin(coeff * time);

		//make circle
		noFill();
		stroke(255);
		ellipse(prevX-200, prevY, radius*2);
		line(prevX-200, prevY, x-200, y);
	}

	wave.unshift(y);

	//cleanup
	if (waveLen++ > 500) {
		wave = wave.slice(0, 400);
		waveLen -= 100;
	}

	//draw past xy locations to form wave
	beginShape();
	noFill();
	//wave.forEach((val, idx) => point(idx, val));
	wave.forEach((val, idx) => vertex(idx+200, val));
	endShape();

	//drawing line from circle to wave
	line(x-200, y, 200, wave[0]);

	time += 0.025;
	if (time >= period) time = 0;
}
