class Droplet {
	// a representation of the magnitude of a 2D wave in a single (x, y) point

	x;
	y;
	screen;
	mag;
	friction;

	constructor(xPos, yPos, screen, friction=0, mag=0) {
		this.x = xPos;
		this.y = yPos;
		this.mag = mag;
		this.friction = friction; // idk what else to call this rn but its the amount of power the wave loses when transmitting to next
	}

	// propegate is called multiple times. the energy of the wave is transported to the next every 'tick'
	// takes multiple ticks for wave to die down and transfer full force in front of self

	// computer will calculate each drop sequentially. you need a separate variable that won't change the 
	// 	magnitude until the next tick, or the magnitude changes will stack within the tick when you only want current

	propegate(prevX, prevY, lastMag) {
		if (mag <= 0) {
			//no more wave, stop
			return;
		}

		// sends wave force in all directions 'forwards' from last pt
		//const dir = (prevY - yPos) / (xPos - prevX); // flip y part b/c screen 0,0 is top left
		const xDir = xPos - prevX;
		const yDir = prevY - yPos;

		// one pixel has eight surrounding it
		// the wave will continue in the five most 'forward' pixels
		// this is gonna need a lot of work when scaling up
		if (xDir > 0 && yDir > 0) {
			// up and right
			this.screen([x+1, y+1, this.mag - this.friction]);
			//TODO: make it go in the other four directions too dummy
			//TODO: even better, make this less clunky
		} else if (xDir > 0 && yDir < 0) {
			// up and right
			this.screen([x+1, y-1, this.mag - this.friction]);
		} else if (xDir > 0 && yDir === 0) {
			// up and right
			this.screen([x+1, y, this.mag - this.friction]);
		} else if (xDir < 0 && yDir > 0) {
			// up and right
			this.screen([x-1, y+1, this.mag - this.friction]);
		} else if (xDir < 0 && yDir < 0) {
			// up and right
			this.screen([x-1, y-1, this.mag - this.friction]);
		} else if (xDir < 0 && yDir === 0) {
			// up and right
			this.screen([x-1, y, this.mag - this.friction]);
		} else if (xDir === 0 && yDir > 0) {
			// up and right
			this.screen([x, y+1, this.mag - this.friction]);
		} else if (xDir === 0 && yDir < 0) {
			// up and right
			this.screen([x, y-1, this.mag - this.friction]);
		} else {
			// prevX === x && prevY === y
			// this is the 'origin drop'. it was the initial condition in the function, so propegate all ways out
		}
			

		// https://stackoverflow.com/questions/9235152/can-i-use-a-case-switch-statement-with-two-variables
		//switch (xDir | yDir) {
			//case xDir > 0 && yDir > 0:
				//// up and right
				//this.screen([x+1, y+1, this.mag - this.friction]);
				//break;
			//case xDir > 0 && yDir < 0:
				//// up and right
				//this.screen([x+1, y-1, this.mag - this.friction]);
				//break;		
			//case xDir > 0 && yDir === 0:
				//// up and right
				//this.screen([x+1, y, this.mag - this.friction]);
				//break;
			//case xDir < 0 && yDir > 0:
				//// up and right
				//this.screen([x-1, y+1, this.mag - this.friction]);
				//break;
			//case xDir < 0 && yDir < 0:
				//// up and right
				//this.screen([x-1, y-1, this.mag - this.friction]);
				//break;
			//case xDir < 0 && yDir === 0:
				//// up and right
				//this.screen([x-1, y, this.mag - this.friction]);
				//break;		
			//case xDir === 0 && yDir > 0:
				// up and right
				//this.screen([x, y+1, this.mag - this.friction]);
				//break;
			//case xDir === 0 && yDir < 0:
				//// up and right
				//this.screen([x, y-1, this.mag - this.friction]);
				//break;
			//default:
				//// prevX === x && prevY === y
				//// this is the 'origin drop'. it was the initial condition in the function, so propegate all ways out
		//}
	}

	adjustColour(magChange) {
		// more blue the bigger the magnitude is
		// red is negative
	}

	draw() {
		// actually draws the droplet
	}
}
