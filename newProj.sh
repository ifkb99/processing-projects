#!/bin/bash
mkdir ./$1
sed s/p5.js\ example/"$1"/g p5/index.html > ./$1/index.html
cp p5/sketch.js ./$1
echo "Project created in $1!"
