//BCs: U(0, t) = U(L, t) = 0
const height = 800;
const width = 800;
const res = width / 20; // amount of approximation for line
let curLY = height/2;
let curRY = height/2;
let t = 0;

function setup() {
	createCanvas(height, width);
}

function draw() {
	// 1d wave
	// initial straight curve
	background(51);
	noFill();
	stroke(255);
	strokeWeight(10);
	// dots approximating the line
	for (let dotIdx = 0; dotIdx < res; dotIdx++) {
		point(dotIdx*res, curLY + res * cos(t + dotIdx));
	}
	t += 0.05;
	if (t >= TWO_PI) t = 0;
	//curve(0, height/2, 0, curLY, 0, curRY, 0, height/2);
	//curve(65, 26, 0, 24, 73, 61, 15, 65);
}
