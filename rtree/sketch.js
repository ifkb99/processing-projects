const width  = 1900;
const height = 1000;
let PIBY4 = 0;
let sway = 0;

function setup() {
	createCanvas(width, height);
	PIBY4 = PI / 4;
}

function branch(x, y, rads, branchSize) {
	if (branchSize >= 5) {
		//get left and right angles for branches
		const newX = x + parseInt(sin(rads) * branchSize);// + (15 / branchSize) * sin(sway);
		const newY = y + parseInt(cos(rads) * branchSize);// + (15 / branchSize) * cos(sway);
		
		line(x, y, newX, newY);

		const newBranch = branchSize * 0.67;
		branch(newX, newY, rads - PIBY4, newBranch);
		branch(newX, newY, rads + PIBY4, newBranch);
	}
}

function draw() {
	background(51);
	stroke(255);
	noFill();
	const nse = noise(map(mouseX, 0, width, 0, 1),
					  map(mouseY, 0, height, 0, 1));
	branch(width / 2, height, PI, nse * 512); 
	//sway += 0.0025;
	PIBY4 = nse;
}
