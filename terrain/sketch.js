const w = 1200;
const h = 900;
const scale = 20;
const cols = w / scale;
const rows = h / scale;

let flyOffset = 0.0;

function setup() {
  // put setup code here
  createCanvas(600, 600, WEBGL);
}

function draw() {
  // put drawing code here
	background(0);
	stroke(255);
	noFill();

	rotateX(PI/3);

	translate(-w/2, -h/2);
	let yOff = flyOffset;
	for (let y = 0; y < rows; y++) {
		beginShape(TRIANGLE_STRIP);
		let xOff = 0.0;
		for (let x = 0; x <= cols; x++) {
			vertex(x * scale, y * scale, map(noise(xOff, yOff), 0, 1, -100, 100));
			vertex(x * scale, (y + 1) * scale, map(noise(xOff, yOff + 0.15), 0, 1, -100, 100));
			xOff += 0.15;
		}
		endShape();
		yOff += 0.15;
	}
	flyOffset -= 0.05;
}
