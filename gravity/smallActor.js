class SmallActor {
	pos;
	vel;
	acc;
	gPlane;

	constructor(xPos, yPos, xVel, yVel, gravPlane) {
		this.pos = createVector(xPos, yPos);
		this.vel = createVector(xVel, yVel);
		this.acc = createVector();
		this.gPlane = gravPlane;
	}

	update() {
		// TODO: get gravity from grav plane
		this.vel.limit(10);
		this.acc.limit(20);

		this.pos.add(this.vel);
		this.vel.add(this.acc);
		//this.acc.add(this.gPlane[Math.floor(this.pos.x / res)][Math.floor(this.pos.y / res)]);

		// set height to grav amt
		this.pos.z = this.gPlane[Math.floor(this.pos.x / res)][Math.floor(this.pos.y / res)];

		// bound checking
		if (this.pos.x >= res * this.gPlane[0].length) {
			this.pos.x = 0;
		} else if (this.pos.x < 0) {
			this.pos.x = res * this.gPlane[0].length;
		}

		if (this.pos.y >= res * this.gPlane.length) {
			this.pos.y = 0;
		} else if (this.pos.y < 0) {
			this.pos.y = res * this.gPlane.length;
		}

		this.acc.mult(0); // reset accel every iteration
	}

	draw() {
		translate(this.pos.x, this.pos.y, this.pos.z);
		sphere(10, 6, 6);
		translate(-this.pos.x, -this.pos.y, -this.pos.z);
		// TODO leave tracer line?
	}
};
