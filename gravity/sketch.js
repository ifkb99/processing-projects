const res = 32;
const width = 800;
const height = 800;
const xRes = width / res;
const yRes = height / res;
const gravPlane = [];

const planets = [];
const smalls  = [];

function setup() {
	createCanvas(width, height, WEBGL);

	noFill();
	background(0);
	stroke(255);

	for (let i = 0; i < res; i++) {
		gravPlane.push(new Float32Array(res));
		//gravPlane.push(new Array(res));
		//for (let j = 0; j < res; j++) {
			//gravPlane[i][j] = createVector();
		//}
	}

	// TODO: figure out proper ratio for mass and density of a typical planet (is there one?)
	// 						   x	  	  y			  mass		     density	   universe
	planets.push(new GravPoint(width / 2, height / 2, 5.972 * 10e14, 5.51 * 10e19, gravPlane));
	//console.log(gravPlane);
	
	//smalls.push(new SmallActor(10, 10, 3, 3, gravPlane, res));
}

let psi = 0;

function draw() {
	background(0);
	rotateX(PI/3);
	translate(-width / 2, -height / 2);

	planets.forEach(planet => planet.update());
	//smalls.forEach(actor => actor.update());

	for (let widthIdx = 0; widthIdx <= res; widthIdx++) {
		const xPos = widthIdx * width / res;
		
		beginShape(TRIANGLE_STRIP);
		for (let heightIdx = 0; heightIdx < res; heightIdx++) {
			const yPos = heightIdx * height / res;
			// TODO: change this when they can attract each other
			vertex(yPos, xPos, gravPlane[heightIdx][widthIdx]);
			vertex(yPos, (widthIdx + 1) * width / res, gravPlane[heightIdx][widthIdx + 1]);
		}
		endShape();
		planets.forEach(planet => planet.draw());
	}

	planets.forEach(planet => planet.addPos(5, 5 * sin(psi) * noise(0, psi)));
	//smalls.forEach(small => small.draw());
	if (psi >= TWO_PI) {
		psi = 0;
	} else {
		psi += 0.05;
	}
}
