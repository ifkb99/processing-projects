class GravPoint {
	mass;
	radius;
	gConst = 6.7e-11;
	x;
	y;

	constructor(x, y, mass, density, universe) {
		this.x = x;
		this.y = y;
		this.mass = mass;
		this.radius = Math.cbrt(density * 0.75 * 1 / (mass * PI)); // radius of sphere
		this.uni = universe; // matrix of world
	}

	update() {
		// Calculates gravitational strength at point
		for (let xIdx = 0; xIdx < res; xIdx++) {
			for (let yIdx = 0; yIdx < res; yIdx++) {
				//const dX = res * xIdx - this.x;
				//const dY = res * yIdx - this.y;
				const d = dist(this.x, this.y, xIdx * xRes, yIdx * yRes);
				//this.uni[xIdx][yIdx].x = Math.max(-(this.gConst * this.mass / (dX * dX)), -500);
				//this.uni[xIdx][yIdx].y = Math.max(-(this.gConst * this.mass / (dY * dY)), -500);
				// z is magnitude to save time
				this.uni[xIdx][yIdx] = Math.max(-(this.gConst * this.mass / (d * d)), -500);
			}
		}
	}

	addPos(x, y) {
		this.x += x;
		this.y += y;

		// width and height are pulled from sketch.js somehow
		if (this.x >= width) {
			this.x -= width;
		} else if (this.x < 0) {
			this.x = width;
		}
		if (this.y >= height) {
			this.y -= height;
		} else if (this.y < 0) {
			this.y = height;
		}
	}

	draw() {
		fill(color(100, 40, 0));
		translate(this.x, this.y); // translate from center to sphere pos
		sphere(this.radius, 8, 8);
		translate(-this.x, -this.y); // translate back to center
		noFill();
	}
};
